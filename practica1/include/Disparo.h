#ifndef _DISPARO_H_
#define _DISPARO_H_

#include "Vector2D.h"

class Disparo
{
public:
	Vector2D centro;
	Vector2D velocidad;
	float radio;

	Disparo();
	~Disparo();

	void Mueve (float t);
	void Dibuja();
};

#endif
