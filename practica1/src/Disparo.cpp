
// Programa para el disparo de la pelota por la raqueta

#include "Disparo.h"
#include "glut.h"

Disparo::Disparo()
{
	radio = 0.1f;
	centro.x=0;
	centro.y=0;
	velocidad.x=0;
	velocidad.y=0;
}

Disparo::~Disparo()
{
 
}

void Disparo::Dibuja()
{
	glColor3ub(255,0,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio, 15, 15);
	glPopMatrix();
}

void Disparo::Mueve(float t)
{
	centro.x += velocidad.x * t;
}
