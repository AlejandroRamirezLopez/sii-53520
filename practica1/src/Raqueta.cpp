// Raqueta.cpp: implementation of the Raqueta class.
// El editor de este archivo es Alejandro Ramirez/
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	congelado = 1;
	velocidad.y = 1;
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	if(congelado >= 1)
	{
		y1 += velocidad.y*t;
		y2 += velocidad.y*t;
	}
	else congelado += 0.01f;
}
